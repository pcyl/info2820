
CREATE TABLE carsharing.Location (
                pk_id INTEGER NOT NULL,
                name VARCHAR NOT NULL,
                type VARCHAR NOT NULL,
                Parent_pk_id INTEGER NOT NULL,
                CONSTRAINT id PRIMARY KEY (pk_id)
);


CREATE TABLE carsharing.CarBay (
                pk_name VARCHAR NOT NULL,
                description VARCHAR,
                longitude VARCHAR NOT NULL,
                lattitude VARCHAR NOT NULL,
                address VARCHAR NOT NULL,
                fk_location_id INTEGER NOT NULL,
                CONSTRAINT name PRIMARY KEY (pk_name)
);


CREATE TABLE carsharing.Car_Model (
                pk_model VARCHAR NOT NULL,
                pk_make VARCHAR NOT NULL,
                category VARCHAR NOT NULL,
                capacity INTEGER NOT NULL,
                CONSTRAINT model_make PRIMARY KEY (pk_model, pk_make)
);


CREATE TABLE carsharing.Car (
                pk_reg_no VARCHAR(6) NOT NULL,
                name VARCHAR NOT NULL,
                year DATE NOT NULL,
                transmission CHAR(1) NOT NULL,
                pk_model VARCHAR NOT NULL,
                pk_make VARCHAR NOT NULL,
                fk_carbay VARCHAR NOT NULL,
                CONSTRAINT regno PRIMARY KEY (pk_reg_no)
);


CREATE TABLE carsharing.Membership_Plan (
                pk_title VARCHAR NOT NULL,
                monthly_fee NUMERIC(2) NOT NULL,
                hourly_rate NUMERIC(2) NOT NULL,
                km_rate NUMERIC(2) NOT NULL,
                daily_rate NUMERIC(2) NOT NULL,
                daily_km_rate NUMERIC(2) NOT NULL,
                daily_km_rate_included NUMERIC(2) NOT NULL,
                CONSTRAINT title PRIMARY KEY (pk_title)
);


CREATE TABLE carsharing.Member (
                pk_email VARCHAR NOT NULL,
                password VARCHAR NOT NULL,
                name_title VARCHAR NOT NULL,
                name_familu VARCHAR NOT NULL,
                name_given VARCHAR NOT NULL,
                license_nr VARCHAR NOT NULL,
                license_expires VARCHAR NOT NULL,
                address VARCHAR NOT NULL,
                nickname VARCHAR NOT NULL,
                since DATE NOT NULL,
                birthdate DATE NOT NULL,
                phone VARCHAR NOT NULL,
                fk_homebay VARCHAR NOT NULL,
                fk_subcription_title VARCHAR NOT NULL,
                fk_preferred_payment INTEGER NOT NULL,
                CONSTRAINT email PRIMARY KEY (pk_email)
);


CREATE TABLE carsharing.Payment_Method (
                pk_num INTEGER NOT NULL,
                pk_email VARCHAR NOT NULL,
                CONSTRAINT pk_num PRIMARY KEY (pk_num)
);


CREATE TABLE carsharing.Credit_Card (
                pfk_num INTEGER NOT NULL,
                name VARCHAR NOT NULL,
                brand VARCHAR NOT NULL,
                number VARCHAR NOT NULL,
                expires DATE NOT NULL,
                CONSTRAINT num_credit PRIMARY KEY (pfk_num)
);


CREATE TABLE carsharing.PayPal (
                pfk_num INTEGER NOT NULL,
                email VARCHAR NOT NULL,
                CONSTRAINT num_paypal PRIMARY KEY (pfk_num)
);


CREATE TABLE carsharing.Bank_Account (
                pfk_num INTEGER NOT NULL,
                name VARCHAR NOT NULL,
                bsb VARCHAR NOT NULL,
                account VARCHAR NOT NULL,
                CONSTRAINT num_bank PRIMARY KEY (pfk_num)
);


CREATE TABLE carsharing.Booking (
                pk_starttime TIMESTAMP NOT NULL,
                pk_email VARCHAR NOT NULL,
                duration TIME NOT NULL,
                when_booked DATE NOT NULL,
                pk_reg_no VARCHAR(6) NOT NULL,
                CONSTRAINT starttime_email PRIMARY KEY (pk_starttime, pk_email)
);


ALTER TABLE carsharing.Location ADD CONSTRAINT location_location_fk
FOREIGN KEY (Parent_pk_id)
REFERENCES carsharing.Location (pk_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE carsharing.CarBay ADD CONSTRAINT location_carbay_fk
FOREIGN KEY (fk_location_id)
REFERENCES carsharing.Location (pk_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE carsharing.Car ADD CONSTRAINT carbay_car_fk
FOREIGN KEY (fk_carbay)
REFERENCES carsharing.CarBay (pk_name)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE carsharing.Car ADD CONSTRAINT car_model_car_fk
FOREIGN KEY (pk_model, pk_make)
REFERENCES carsharing.Car_Model (pk_model, pk_make)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE carsharing.Booking ADD CONSTRAINT car_booking_fk
FOREIGN KEY (pk_reg_no)
REFERENCES carsharing.Car (pk_reg_no)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE carsharing.Member ADD CONSTRAINT membership_plan_member_fk
FOREIGN KEY (fk_subcription_title)
REFERENCES carsharing.Membership_Plan (pk_title)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE carsharing.Booking ADD CONSTRAINT member_booking_fk
FOREIGN KEY (pk_email)
REFERENCES carsharing.Member (pk_email)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE carsharing.Payment_Method ADD CONSTRAINT member_payment_method_fk
FOREIGN KEY (pk_email)
REFERENCES carsharing.Member (pk_email)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE carsharing.Member ADD CONSTRAINT payment_method_member_fk
FOREIGN KEY (fk_preferred_payment)
REFERENCES carsharing.Payment_Method (pk_num)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE carsharing.Bank_Account ADD CONSTRAINT payment_method_bank_account_fk
FOREIGN KEY (pfk_num)
REFERENCES carsharing.Payment_Method (pk_num)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE carsharing.PayPal ADD CONSTRAINT payment_method_paypal_fk
FOREIGN KEY (pfk_num)
REFERENCES carsharing.Payment_Method (pk_num)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE carsharing.Credit_Card ADD CONSTRAINT payment_method_credit_card_fk
FOREIGN KEY (pfk_num)
REFERENCES carsharing.Payment_Method (pk_num)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
